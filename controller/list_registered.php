<?php
require_once __DIR__ . "/../resource/database.php";

$customerIDs = getCustomerList(TRUE);
if(!empty($customerIDs)){
	$customerData = getCustomerDetails($customerIDs);
	$tableHead = array_keys($customerData[$customerIDs[0]]);

	$pageTitle = $pageName . ": Registered Accounts";
	require_once __DIR__ . "/../view/list_registered.php";
} else {
	require_once __DIR__ . "/../view/database_error.php";
}

?>