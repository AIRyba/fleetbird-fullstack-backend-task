<?php
require_once __DIR__ . "/../config/register_config.php";
require_once __DIR__ . "/../resource/database.php";
require_once __DIR__ . "/../resource/api.php";

for($i=1; $i<=$numberOfSteps; $i++){
	foreach($fieldsInStep[$i] as $formFieldKey){
		$$formFieldKey = fetchParam($formFieldKey, "String", "POST", "");
	}
}

// Kunden anlegen & CustomerID holen
$customerID = addCustomer($firstName, $lastName, $phone);
$_SESSION['customerID'] = $customerID;
$_COOKIE['customerID'] = $customerID;

// API ansprechen
$paymentID = getPaymentDataID($customerID, $iban, $accountOwner);
$_SESSION['paymentID'] = $paymentID;
$_COOKIE['paymentID'] = $paymentID;

// Adresse anlegen
$addressID = addAddress($customerID, $street, $houseNo, $zipCode, $city);

// Payment-Info anlegen
$paymentInfoID = addPaymentInfo($customerID, $accountOwner, $iban, $paymentID);

// Erfolg überprüfen
if( !empty($customerID) && !empty($paymentID) && !empty($addressID) && !empty($paymentInfoID)){
	require_once __DIR__ . "/../view/success.php";
} else {
	require_once __DIR__ . "/../view/failure.php";
}

?>