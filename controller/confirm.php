<?php

// Testen, ob Kunden-ID und Payment-Info vorhanden sind
$customerID = ((!empty($_SESSION['customerID'])) ? $_SESSION['customerID'] : NULL);
if(empty($customerID)) $customerID = ((!empty($_COOKIE['customerID'])) ? $_COOKIE['customerID'] : NULL);

$paymentID = ((!empty($_SESSION['paymentID'])) ? $_SESSION['paymentID'] : NULL);
if(empty($paymentID)) $paymentID = ((!empty($_COOKIE['paymentID'])) ? $_COOKIE['paymentID'] : NULL);

if((!empty($customerID)) && (!empty($paymentID))) $registrationCompleted = TRUE;
else $registrationCompleted = FALSE;

require_once __DIR__ . "/../view/confirm.php";

?>