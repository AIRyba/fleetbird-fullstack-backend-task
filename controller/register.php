<?php
require_once __DIR__ . "/../config/register_config.php";

// Höchsten abgeschlossenen Schritt aus den Session / Cookie-Daten holen
$lastRegistrationStep = ((!empty($_SESSION[$currentStepField])) ? $_SESSION[$currentStepField] : 0);
if($lastRegistrationStep == 0) $lastRegistrationStep = ((!empty($_COOKIE[$currentStepField])) ? $_COOKIE[$currentStepField] : 0);

sanitizeInteger($lastRegistrationStep, 0, $numberOfSteps, 0);

// Fehlende Session-Daten mit Cookie-Daten auffüllen
for($i = $lastRegistrationStep; $i >0; $i--){
	foreach($fieldsInStep[$i] as $fieldName){
		if(empty($_SESSION[$fieldName])){
			$_SESSION[$fieldName] = ((!empty($_COOKIE[$fieldName])) ? $_COOKIE[$fieldName] : NULL);
		}
	}
}

// echo "<br> --- LastStep: $lastRegistrationStep --- <br>";

if($_SERVER['REQUEST_METHOD'] == "POST"){
	
	// Absolvierte Seite aus Post-Parameter holen
	$completedStep = fetchParam($currentStepField, "int", "POST", 0);
	// echo "<br> --- Completed: $completedStep --- <br>";
	sanitizeInteger($completedStep, 1, $numberOfSteps, 0);
	// echo "<br> --- Completed: $completedStep --- <br>";

	if($completedStep > $lastRegistrationStep){
		$_COOKIE[$currentStepField] = $completedStep;
		$_SESSION[$currentStepField] = $completedStep;
	}

	// Weitere POST-Felder auslesen
	if($completedStep > 0){
		foreach($fieldsInStep[$completedStep] as $fieldName){
			$_SESSION[$fieldName] = fetchParam($fieldName, "String", "POST", "");
			$_COOKIE[$fieldName] = $_SESSION[$fieldName];
		}
	}

	// Anzuzeigende Seite holen
	if(!empty($_POST["back"]) && $completedStep > 1) $thisStep = ($completedStep - 1);
	else $thisStep =  ($completedStep + 1);

} else {
	$nextRegistrationStep = ( ($lastRegistrationStep < ($numberOfSteps+1)) ? ($lastRegistrationStep + 1) : $lastRegistrationStep);
	// echo "<br> --- nextRegistrationStep: $nextRegistrationStep --- <br>";
	$thisStep = fetchParam("s", "int", "GET", $nextRegistrationStep);
	// echo "<br> --- thisStep: $thisStep --- <br>";
	
	sanitizeInteger($thisStep, 1, $nextRegistrationStep, $nextRegistrationStep);
	// echo "<br> --- thisStep: $thisStep --- <br>";
}

if($thisStep <= $numberOfSteps)
	require_once __DIR__ . "/../view/register.php";
else 
	require_once __DIR__ . "/../controller/confirm.php";

?>