<?php

$defaultMetaArray[] = array("http-equiv" => "refresh", "content" => "5; URL=index.php");

$breadcrumbs[] = array(
	'name' => "Database Error",
	'active' => FALSE,
	'target' => NULL
);

$pageTitle = $pageName . ": Database Error";

require_once __DIR__ . "/../view/database_error.php";

?>