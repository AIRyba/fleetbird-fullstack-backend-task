<?php

$defaultMetaArray[] = array("http-equiv" => "refresh", "content" => "5; URL=index.php");

$breadcrumbs[] = array(
	'name' => "Error",
	'active' => FALSE,
	'target' => NULL
);

$pageTitle = $pageName . ": Error";

require_once __DIR__ . "/../view/error.php";

?>