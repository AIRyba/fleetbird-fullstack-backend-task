<?php 
require_once __DIR__ . "/config/config.php";
require_once __DIR__ . "/resource/web_helper.php";

$targetSite = fetchParam("page", "String", NULL, "index");

switch($targetSite){
	case 'register': $controller = "register.php"; break;
	case 'confirm': $controller = "confirm.php"; break;
	case 'submit': $controller = "submit.php"; break;
	case 'index': $controller = "index.php"; break;
	case 'registered': $controller = "list_registered.php"; break;
	default: $controller = "error.php"; break;
}

require_once __DIR__ . "/controller/" . $controller;

?>