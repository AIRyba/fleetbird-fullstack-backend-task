<?php

$serverName = trim(strtolower(php_uname('n')));
switch($serverName){
	case 'ginnungagap': $iniFile = 'dev.ini'; break;
	default: $iniFile = 'server.ini'; break; 
}

$iniFile = __DIR__ . "/" . $iniFile;

if(file_exists($iniFile))
	$dbIni = parse_ini_file($iniFile);
else 
	require_once __DIR__ . "/../controller/database_error.php";

?>