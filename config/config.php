<?php
session_start();

$pageName = "Fleetbird Test";
$pageLanguage = "en";
$breadcrumbDelimiter = ">";

$defaultMetaArray = array();
$defaultLinkArray = array(
	array('rel' => 'stylesheet', 'href' => 'layout.css')
);


$breadcrumbs = array();
$breadcrumbs[] = array(
	'name' => $pageName,
	'active' => TRUE,
	'target' => 'index.php'
);

?>