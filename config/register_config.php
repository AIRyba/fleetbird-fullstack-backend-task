<?php

$sectionName = "Registration";

$stepNames = array(
	1 => "Personal",
	2 => "Address",
	3 => "Payment"
);

$fieldsInStep = array(
	1 => array("First Name" => 'firstName', "Last Name" => 'lastName', "Phone Number" => 'phone'),
	2 => array("Street" => 'street', "House Number" => 'houseNo', "Zipcode" => 'zipCode', "City" => 'city'),
	3 => array("Account Owner" => 'accountOwner', "IBAN" => 'iban')
);

$numberOfSteps = sizeof($stepNames);
$currentStepField = "step";

?>