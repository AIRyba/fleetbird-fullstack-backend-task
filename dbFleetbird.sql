/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE IF NOT EXISTS `dbFleetbird` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `dbFleetbird`;

CREATE TABLE IF NOT EXISTS `tAddress` (
  `iAddressID` int(11) NOT NULL AUTO_INCREMENT,
  `iCustomerID` int(11) NOT NULL,
  `vStreet` varchar(250) NOT NULL,
  `vHouseNo` varchar(10) NOT NULL,
  `vZipCode` varchar(10) NOT NULL,
  `vCity` varchar(100) NOT NULL,
  PRIMARY KEY (`iAddressID`),
  KEY `FK_tAddress_tCustomer` (`iCustomerID`),
  CONSTRAINT `FK_tAddress_tCustomer` FOREIGN KEY (`iCustomerID`) REFERENCES `tCustomer` (`iCustomerID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

DELETE FROM `tAddress`;
/*!40000 ALTER TABLE `tAddress` DISABLE KEYS */;
INSERT INTO `tAddress` (`iAddressID`, `iCustomerID`, `vStreet`, `vHouseNo`, `vZipCode`, `vCity`) VALUES
	(1, 1, 'Musterstr', '666', '4862', 'Musterstadt');
/*!40000 ALTER TABLE `tAddress` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `tCustomer` (
  `iCustomerID` int(11) NOT NULL AUTO_INCREMENT,
  `vFirstName` varchar(150) NOT NULL,
  `vLastName` varchar(150) NOT NULL,
  `vPhone` varchar(150) NOT NULL,
  PRIMARY KEY (`iCustomerID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

DELETE FROM `tCustomer`;
/*!40000 ALTER TABLE `tCustomer` DISABLE KEYS */;
INSERT INTO `tCustomer` (`iCustomerID`, `vFirstName`, `vLastName`, `vPhone`) VALUES
	(1, 'Max', 'Mustermann', '123 - 456 789');
/*!40000 ALTER TABLE `tCustomer` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `tPaymentInfo` (
  `iPaymentInfoID` int(11) NOT NULL AUTO_INCREMENT,
  `iCustomerID` int(11) NOT NULL,
  `vAccountOwner` varchar(300) NOT NULL,
  `vIBAN` varchar(50) NOT NULL,
  `vPaymentDataID` varchar(300) DEFAULT NULL COMMENT 'Hexadezimal, varchar nicht nötig',
  PRIMARY KEY (`iPaymentInfoID`),
  KEY `FK_tPaymentInfo_tCustomer` (`iCustomerID`),
  CONSTRAINT `FK_tPaymentInfo_tCustomer` FOREIGN KEY (`iCustomerID`) REFERENCES `tCustomer` (`iCustomerID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

DELETE FROM `tPaymentInfo`;
/*!40000 ALTER TABLE `tPaymentInfo` DISABLE KEYS */;
INSERT INTO `tPaymentInfo` (`iPaymentInfoID`, `iCustomerID`, `vAccountOwner`, `vIBAN`, `vPaymentDataID`) VALUES
	(1, 1, 'Max Mustermann', 'DE12 5589 3345 7785 992', 'bbc1c419bb513182f6c322d6bef24f132c8ef2de6cbbf64efaf6e67a51c3763315b484ad647987a5609375885a687352');
/*!40000 ALTER TABLE `tPaymentInfo` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
