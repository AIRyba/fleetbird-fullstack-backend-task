<?php

// HTML-Header
$pageTitle = $pageName . ": Main Page";
echo makeHead($pageName, $pageLanguage, $defaultMetaArray, $defaultLinkArray);

// Seiten-Header
echo "<h2>" . htmlentities($pageTitle) . "</h2>";

// Breadcrumbs
echo "<hr>" . makeBreadcrumbs($breadcrumbs, $breadcrumbDelimiter) . "<br><br><hr>";

echo "
	<a href=\"?page=register\">Register here for our amazing Service!</a><br>
	<a href=\"?page=registered\">See who else has registered!</a><br>
";

// Ende
echo "\r\n\t</body>\r\n</html>";
?>