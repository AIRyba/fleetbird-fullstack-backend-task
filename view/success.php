<?php

// HTML-Header
$pageTitle = $pageName . ": Registration Successful";
echo makeHead($pageName, $pageLanguage, $defaultMetaArray, $defaultLinkArray);

// Seiten-Header
echo "<h2>" . htmlentities($pageTitle) . "</h2>";

// Breadcrumbs
echo "<hr>" . makeBreadcrumbs($breadcrumbs, $breadcrumbDelimiter) . "<br><br><hr>";

// Body
echo "Thank you for registering at our awesome Service!<br>\r\n";
$tableBody = array(
	array("<b>Customer-ID:</b>", $_SESSION['customerID']),
	array("<b>Payment-ID:</b>", $_SESSION['paymentID'])
);
echo arrayToTable($tableBody, NULL, TRUE)."\r\n";

// Ende
echo "\r\n\t</body>\r\n</html>";
?>