<?php

// HTML-Header
echo makeHead($pageTitle, $pageLanguage, $defaultMetaArray, $defaultLinkArray);

// Seiten-Header
echo "<h2>" . htmlentities($pageTitle) . "</h2>";

// Breadcrumbs
$breadcrumbs[] = array(
	'name' => "Registered Accounts",
	'active' => TRUE,
	'target' => '?page=registered'
);
echo "<hr>" . makeBreadcrumbs($breadcrumbs, $breadcrumbDelimiter) . "<br><br><hr>";


// Body
echo arrayToTable($customerData, $tableHead, TRUE, TRUE);

// Ende
echo "\r\n\t</body>\r\n</html>";

?>