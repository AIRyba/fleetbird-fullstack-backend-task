<?php

// HTML-Header
$pageTitle = $pageName . ": Hauptseite";
echo makeHead($pageName, $pageLanguage, $defaultMetaArray, $defaultLinkArray);

// Seiten-Header
echo "<h2>" . htmlentities($pageTitle) . "</h2>";

// Breadcrumbs
echo "<hr>" . makeBreadcrumbs($breadcrumbs, $breadcrumbDelimiter) . "<br><br><hr>";


// Ende
echo "\r\n\t</body>\r\n</html>";

?>