<?php
// HTML-Header
echo makeHead($pageTitle, $pageLanguage, $defaultMetaArray, $defaultLinkArray);

// Seiten-Header
echo "<h2>" . htmlentities($pageTitle) . "</h2>";

// Breadcrumbs
echo "<hr>" . makeBreadcrumbs($breadcrumbs, $breadcrumbDelimiter) . "<br><br><hr>";

// Body
echo "<h3>The Site you requested could not be found!</h3><br>\r\n";
echo "<img src=\"resource/images/kitten.jpg\" alt=\"hereBeKittens\"><br>\r\n";
echo "Kitten has no idea why<br>\r\n";

// Ende
echo "\r\n\t</body>\r\n</html>";
?>