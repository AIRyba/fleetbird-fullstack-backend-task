<?php

// HTML-Header
$subName = $sectionName . " (" . $thisStep . "/" . ($numberOfSteps+1) . ") - " . $stepNames[$thisStep] . " Information";
$pageTitle = $pageName . ": " . $subName;
echo makeHead($pageTitle, $pageLanguage, $defaultMetaArray, $defaultLinkArray);

// Seiten-Header
echo "<h2>" . htmlentities($subName) . "</h2>";

// Breadcrumbs
for($i=1; $i<=$numberOfSteps; $i++){
	$breadcrumbs[] = array(
		'name' => $stepNames[$i] . " Information",
		'active' => ($i <= ($lastRegistrationStep + 1)),
		'target' => '?page=register&s='.$i
	);
}
$breadcrumbs[] = array(
	'name' => "Confirmation",
	'active' => ($lastRegistrationStep == $numberOfSteps),
	'target' => '?page=register&s='.($numberOfSteps+1)
);
echo "<hr>" . makeBreadcrumbs($breadcrumbs, $breadcrumbDelimiter) . "<br><br><hr>";

// Formular
echo "
<form action=\"index.php\" method=\"POST\">
	<input type=\"hidden\" name=\"page\" value=\"register\">
	<input type=\"hidden\" name=\"".$currentStepField."\" value=\"".$thisStep."\">
";

$tableBody = array();
foreach($fieldsInStep[$thisStep] as $formFieldName => $formFieldKey){
	$tableBody[] = array(
		"<b>" . htmlentities($formFieldName) . "</b>",
		"<input type=\"text\" name=\"" . $formFieldKey . "\" value=\"" . htmlspecialchars($_SESSION[$formFieldKey]) . "\" placeholder=\"" . htmlentities($formFieldName) . "\" required>"
	);
}
$tableBody[] = array(
	(($thisStep > 1) ? "<input type=\"submit\" name=\"back\" value=\"Back\">" :""),
	"<input type=\"submit\" name=\"next\" value=\"Next\">"
);

echo arrayToTable($tableBody, NULL, TRUE);

echo "
</form>
";

// Ende
echo "\r\n\t</body>\r\n</html>";

?>