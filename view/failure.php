<?php

// HTML-Header
$pageTitle = $pageName . ": Registration Failed";
echo makeHead($pageName, $pageLanguage, $defaultMetaArray, $defaultLinkArray);

// Seiten-Header
echo "<h2>" . htmlentities($pageTitle) . "</h2>";

// Breadcrumbs
echo "<hr>" . makeBreadcrumbs($breadcrumbs, $breadcrumbDelimiter) . "<br><br><hr>";

// Body
if(empty($customerID)) echo "Could not create your Customer Account!<br>\r\n";
if(empty($addressID)) echo "Could not create your Address!<br>\r\n";
if(empty($paymentInfoID)) echo "Could not create your payment Info!<br>\r\n";
if(empty($paymentID)) echo "Could not complete your Payment!<br>\r\n";

// Ende
echo "\r\n\t</body>\r\n</html>";
?>