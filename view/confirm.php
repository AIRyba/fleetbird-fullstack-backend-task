<?php

// HTML-Header
$subName = $sectionName . " (" . $thisStep . "/" . ($numberOfSteps+1) . ") - Confirmation";
$pageTitle = $pageName . ": " . $subName;
echo makeHead($pageTitle, $pageLanguage, $defaultMetaArray, $defaultLinkArray);

// Seiten-Header
echo "<h2>" . htmlentities($subName) . "</h2>";

// Breadcrumbs
for($i=1; $i<=$numberOfSteps; $i++){
	$breadcrumbs[] = array(
		'name' => $stepNames[$i] . " Information",
		'active' => ($i <= ($lastRegistrationStep + 1)),
		'target' => '?page=register&s='.$i
	);
}
$breadcrumbs[] = array(
	'name' => "Confirmation",
	'active' => ($lastRegistrationStep == $numberOfSteps),
	'target' => '?page=register&s='.($numberOfSteps+1)
);
echo "<hr>" . makeBreadcrumbs($breadcrumbs, $breadcrumbDelimiter) . "<br><br><hr>\r\n";

// Senden der Daten via POST statt über die Session-Variablen, damit submit auch als API genutzt werden kann
if(!$registrationCompleted){
	echo "
		<form action=\"index.php\" method=\"POST\">\r\n
		<input type=\"hidden\" name=\"page\" value=\"submit\">	
	";
}

// Tabelle mit eingegebenen Daten
$tableBody = array();
for($i=1; $i<=$numberOfSteps; $i++){
	foreach($fieldsInStep[$i] as $formFieldName => $formFieldKey){
		$subArray = array(
			"<b>" . htmlentities($formFieldName) . "</b>",
			htmlspecialchars($_SESSION[$formFieldKey])
		);
		if(!$registrationCompleted){
			$subArray[] = "<input type=\"hidden\" name=\"".$formFieldKey."\" value=\"". htmlspecialchars($_SESSION[$formFieldKey]) ."\">";
		}
		$tableBody[] = $subArray;
	}
}

if(!$registrationCompleted){
	$tableBody[] = array(
		"",
		"<input type=\"submit\" name=\"next\" value=\"Complete Registration\">"
	);
} else {
	$tableBody[] = array(
		"<b>Customer ID</b>",
		htmlspecialchars($customerID)
	);
	$tableBody[] = array(
		"<b>Payment Data ID</b>",
		htmlspecialchars($paymentID)
	);
}
echo arrayToTable($tableBody, NULL, TRUE)."\r\n";

if(!$registrationCompleted) echo "</form>\r\n";

// Ende
echo "\r\n\t</body>\r\n</html>";

?>