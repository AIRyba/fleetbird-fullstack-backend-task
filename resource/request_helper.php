<?php

function makeRequest($method, $data, $useJson = TRUE){
	$data = (($useJson) ? json_encode($data) : http_build_query($data) );
	$contentType = (($useJson) ? "json" : "x-www-form-urlencoded" );

	$request = array(
		'http' => array(
			'header'  => "Content-type:application/" . $contentType . "\r\n". 
						 "Content-Length: " . strlen($data) . "\r\n",
			'method'  => $method,
			'content' => $data
		)
	);

	return $request;
}

function requestJsonFromApi($request, $url){
	$response = NULL;

	// Request absenden
	$context = stream_context_create($request);
	$stream = fopen($url, 'r', false, $context);

	if(streamHasResponseCode($stream, 200)){
		// Antwort holen
		$response = stream_get_contents($stream);
		$response = json_decode($response, TRUE);
		fclose($stream);
	}

	return $response;
}

function streamHasResponseCode(&$stream, $expectedResponseCode = 200){
	$responseCodeRegex = "/\b[1-59]\d\d\b/u";
	if(!is_resource($stream)) return FALSE;

	// Response-Code holen
	$metaData = stream_get_meta_data($stream);
	$responseCode = $metaData["wrapper_data"][0];
	if(preg_match($responseCodeRegex, $responseCode, $matches) == 1){
		$responseCode = array_shift($matches);
		unset($matches);
	}

	return (intval($responseCode) == $expectedResponseCode);
}

?>