<?php

require_once __DIR__ . '/request_helper.php';

function getPaymentDataID($customerID, $iban, $owner){
	require __DIR__ ."/../config/api_config.php";

	$answerKey = "paymentDataId";
	$paymentDataID = NULL;

	// Request vorbereiten
	$body = array(
		'customerId' => $customerID,
		'iban'		 => $iban,
		'owner'		 => $owner
	);

	$request = makeRequest($apiMethod, $body);
	$response = requestJsonFromApi($request, $apiUrl);

	// Payment-Data-ID holen
	if($response !== FALSE){
		$paymentDataID = $response[$answerKey];
	}

	return $paymentDataID;
}

?>