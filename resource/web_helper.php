<?php 

function fetchParam($name, $filter='string', $method=NULL, $default=NULL){
	$result = '';

	// Filter holen
	switch(strtolower($filter)){
		case 'string':
			$input_filter = FILTER_SANITIZE_STRING;
		break;
		case 'url':
			$input_filter = FILTER_SANITIZE_URL;
		break;
		case 'mail':
			$input_filter = FILTER_SANITIZE_EMAIL;
		break;
		case 'int':
			$input_filter = FILTER_SANITIZE_NUMBER_INT;
		break;
		case 'float':
			$input_filter = FILTER_SANITIZE_NUMBER_FLOAT;
		break;
		case 'unsafe':
			$input_filter = FILTER_UNSAFE_RAW;
		break;
		default:
			$input_filter = FILTER_SANITIZE_STRING;
		break;
	} // switch

	$method = (empty($method) ? $_SERVER['REQUEST_METHOD'] : $method);
	$method = trim($method);
	$method = substr($method,0,1);
	$method = strtolower($method);

	switch($method){
		case 'g':
			$result = ((empty($_GET[$name])) ? $default : filter_input(INPUT_GET, $name, $input_filter));
		break;
		
		case 'p':
			$result = ((empty($_POST[$name])) ? $default : filter_input(INPUT_POST, $name, $input_filter));
		break;
		
		default:
			$result = $default;
		break;
	}

	return $result;
}

function sanitizeInteger(&$value, $min = NULL, $max = NULL, $default = NULL){
	$value = trim($value);
	if( !is_numeric($value) || ($value < $min) || ($value > $max) ) $value = $default;
	else $value = intval($value);
}

function makeHead($pageTitle, $language = NULL, $meta = NULL, $link = NULL){
	$head = "<!doctype php>\r\n";
	$head .= "<html" . ((!empty($language)) ? " lang=\"" . htmlspecialchars($language) . "\"" : "").">\r\n";
	$head .= "\t<head>\r\n";
	$head .= "\t\t<meta charset=\"utf-8\">\r\n";

	if(is_array($meta)){
		foreach($meta as $element){
			$line = "\t\t<meta";
			foreach($element as $key => $value){
				$line .= " " . htmlspecialchars($key) . "=\"" . htmlspecialchars($value) . "\"";
			}
			$line .= " >\r\n";
			$head .= $line;
		}
	}

	if(is_array($link)){
		foreach($link as $element){
			$line = "\t\t<link";
			foreach($element as $key => $value){
				$line .= " " . htmlspecialchars($key) . "=\"" . htmlspecialchars($value) . "\"";
			}
			$line .= " >\r\n";
			$head .= $line;
		}
	}

	$head .= "\t\t<title>".htmlentities($pageTitle)."</title>\r\n";
	$head .= "\t</head>\r\n";
	$head .= "\t<body>\r\n";

	return $head;
}


function makeBreadcrumbs($array, $token = "=>"){
	$breadcrumbs = "<div class=\"breadcrumb_container\">\r\n";

	if(is_array($array)){
		$arraySize = sizeof($array);
		$counter = 0;
		foreach($array as $crumb){
			$counter++;
			$crumbText = "<div class=\"breadcrumb_element_".(($crumb['active']) ? "enabled" : "disabled")."\">\r\n\t\t";
			$crumbText .= (($crumb['active']) ? "<a href=\"" . $crumb['target'] . "\">" : "") . htmlentities($crumb['name']) . (($crumb['active']) ? "</a>" : "") . "\r\n";
			$crumbText .= "\t</div>\r\n";
			$breadcrumbs .= "\t".$crumbText."\r\n";
			if($counter < $arraySize){
				$breadcrumbs .= "\t<div class=\"breadcrumb_token\"> " . htmlentities($token) . " </div>\r\n";
			}
		}
	}

	$breadcrumbs .= "</div>";
	return $breadcrumbs;
}

function arrayToTable($body, $head = NULL, $useDiv = TRUE, $escape = FALSE){
	$table = (($useDiv)? "<div class=\"divTable\">" : "<table>") . "\r\n";

	if(is_array($head)){
		$tableHead = "\t" . (($useDiv)? "<div class=\"divTableHeading\">" : "<thead>") . "\r\n";
		$tableHead .= "\t\t" . (($useDiv)? "<div class=\"divTableRow\">" : "<tr>") . "\r\n";
		foreach($head as $colName){
			$tableHead .= "\t\t\t" . (($useDiv)? "<div class=\"divTableHead\">" : "<th>") . $colName . (($useDiv)? "</div>" : "</th>") . "\r\n";
		}
		$tableHead .= "\t\t" . (($useDiv)? "</div>" : "</tr>") . "\r\n";
		$tableHead .= "\t" . (($useDiv)? "</div>" : "</thead>") . "\r\n";
	} else {
		$tableHead = "";
	}

	if(is_array($body)){
		$tableBody = "\t" . (($useDiv)? "<div class=\"divTableBody\">" : "<tbody>") . "\r\n";
		foreach($body as $row){
			$tableBody .= "\t\t" . (($useDiv)? "<div class=\"divTableRow\">" : "<tr>") . "\r\n";
			foreach($row as $cell){
				$tableBody .= "\t\t\t" . (($useDiv)? "<div class=\"divTableCell\">" : "<td>") . (($escape) ? htmlentities($cell) : $cell) . (($useDiv)? "</div>" : "</td>") . "\r\n";
			}
			$tableBody .= "\t\t" . (($useDiv)? "</div>" : "</tr>") . "\r\n";
		}
		$tableBody .= "\t" . (($useDiv)? "</div>" : "</tbody>") . "\r\n";
	} else {
		$tableBody = "";
	}

	$table .= $tableHead . $tableBody . (($useDiv)? "</div>" : "</table>");
	return $table;
}


?>