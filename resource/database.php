<?php

function addCustomer($firstName, $lastName, $phone){
	require __DIR__ . "/mysqli_connect.php";
	$query = "
		INSERT INTO ".$dbIni['db'].".tCustomer (vFirstName, vLastName, vPhone)
		VALUES (?, ?, ?);
	";

	$customerID = NULL;
	if(mysqli_stmt_prepare($state, $query)){
		mysqli_stmt_bind_param($state, "sss", $firstName, $lastName, $phone);
		if(mysqli_stmt_execute($state)){
			$customerID = mysqli_insert_id($con);
		}
	} 
	mysqli_stmt_close($state);
	mysqli_close($con);

	return $customerID;
}

function addAddress($customerID, $strest, $houseNo, $zipCode, $city){
	require __DIR__ . "/mysqli_connect.php";
	$query = "
		INSERT INTO ".$dbIni['db'].".tAddress (iCustomerID, vStreet, vHouseNo, vZipCode, vCity)
		VALUES (?, ?, ?, ?, ?);
	";

	$addressID = NULL;
	if(mysqli_stmt_prepare($state, $query)){
		mysqli_stmt_bind_param($state, "issss", $customerID, $strest, $houseNo, $zipCode, $city);
		if(mysqli_stmt_execute($state)){
			$addressID = mysqli_insert_id($con);
		}
	} 
	mysqli_stmt_close($state);
	mysqli_close($con);

	return $addressID;
}

function addPaymentInfo($customerID, $owner, $iban, $paymentID){
	require __DIR__ . "/mysqli_connect.php";
	$query = "
		INSERT INTO ".$dbIni['db'].".tPaymentInfo (iCustomerID, vAccountOwner, vIBAN, vPaymentDataID)
		VALUES (?, ?, ?, ?);
	";

	$paymentInfoID = NULL;
	if(mysqli_stmt_prepare($state, $query)){
		mysqli_stmt_bind_param($state, "isss", $customerID, $owner, $iban, $paymentID);
		if(mysqli_stmt_execute($state)){
			$paymentInfoID = mysqli_insert_id($con);
		}
	} 
	mysqli_stmt_close($state);
	mysqli_close($con);

	return $paymentInfoID;
}

function getCustomerList($onlyIDs = FALSE){
	require __DIR__ . "/mysqli_connect.php";
	$query = "
		SELECT
			c.iCustomerID,
			c.vFirstName, 
			c.vLastName
		FROM ".$dbIni['db'].".tCustomer c;
	";

	$result = array();

	if(mysqli_stmt_prepare($state, $query)){
		mysqli_stmt_execute($state);
		mysqli_stmt_bind_result($state, $customerID, $firstName, $lastName);
		while(mysqli_stmt_fetch($state)){
			if($onlyIDs){
				$result[] =  $customerID;
			} else {
				$result[] = array(
					'customerID' 	=> $customerID,
					'firstName' 	=> $firstName,
					'lastName'		=> $lastName
				);
			}
		}
	}
	mysqli_stmt_close($state);
	mysqli_close($con);

	return $result;
}

function getCustomerDetails($customerIDs){
	require __DIR__ . "/mysqli_connect.php";

	$customerID = NULL;
	if(!is_array($customerIDs)){
		$customerIDs = array($customerIDs);
	}

	$query = "
		SELECT
			c.vFirstName, 
			c.vLastName,
			c.vPhone,
			a.vStreet, 
			a.vHouseNo, 
			a.vZipCode, 
			a.vCity,
			p.vAccountOwner, 
			p.vIBAN, 
			p.vPaymentDataID
		FROM ".$dbIni['db'].".tCustomer c
		LEFT JOIN ".$dbIni['db'].".tAddress a
		ON a.iCustomerID = c.iCustomerID
		LEFT JOIN ".$dbIni['db'].".tPaymentInfo p
		ON p.iCustomerID = c.iCustomerID
		WHERE c.iCustomerID = ?;
	";

	$result = array();

	if(mysqli_stmt_prepare($state, $query)){
		mysqli_stmt_bind_param($state, "i", $customerID);
		foreach($customerIDs as $customerID){
			mysqli_stmt_execute($state);
			mysqli_stmt_bind_result($state, $firstName, $lastName, $phone, $street, $houseNo, $zipCode, $city, $owner, $iban, $paymentID);
			mysqli_stmt_fetch($state);
			$result[$customerID] = array(
				'customerID' => $customerID,	// Für einfachere Tabellen
				'firstName' => $firstName,		// Für einfachere Tabellen
				'lastName'	=> $lastName,		// Für einfachere Tabellen
				'phone' 	=> $phone,
				'street' 	=> $street,
				'houseNo'	=> $houseNo,
				'zipCode'	=> $zipCode,
				'city'		=> $city,
				'owner'		=> $owner,
				'iban'		=> $iban,
				'paymentID'	=> $paymentID
			);
		}
	}
	mysqli_stmt_close($state);
	mysqli_close($con);

	return $result;
}


?>