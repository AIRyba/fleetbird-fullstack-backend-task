

Gliederung der Readme

1 - Performance-Optimierungen
2 - Verbesserungen
	2.1 - Datenbank
		2.1.1 - Städte
		2.1.2 - Postleitzahlen
		2.1.3 - Adressen & Zahlungsoptionen
		2.1.4 - Länder
	2.2 - Front-End
		2.2.1 - Registrierung
		2.2.2 - Eingabe-Verifikation & Bereinigung
		2.2.3 - Anzeige
		2.2.4 - Multi-Language
3 - Implementierung

=======================================================================================

1 - Performance-Optimierungen
Die Verwendung eines Frameworks würde die Arbeit an der Seite erweitern und den bestehenden Code durch geschicktes Caching schneller werden lassen.

=======================================================================================

2 - Verbesserungen

2.1 - Datenbank
Die Datenbank ist zu diesem Zeitpukt noch sehr unausgeprägt. Folgende Dinge wären denkbar:

2.1.1 - Städte
In der Datenbank könnte eine Liste mit Städten hinterlegt werden, welche bei der Eingabe des Nutzers mittels JS angefragt wird und das Text-Eingabe-Feld um einen Dropdown mit bereits bekannten Städten erweitert. Beim Absenden der Daten wird geprüft, ob eine Stadt-ID aus dem Dropdown oder aber ein String übergeben wurden. In letzterem Fall würde mittels einer Kombination aus Vereinheitlichung und Levensthein-Funktion noch einmal ein Abgleich gegen die Datenbank gefahren um Tippfehler auszuschließen, und ansonsten die Stadt der Datenbank hinzugefügt werden.

2.1.2 - Postleitzahlen
In der Datenbank könnte eine Liste aller bekannten Postleitzahlen sowie ein Mapping dieser zu den IDs der Städte hinterlegt werden. Gibt der Nutzer im UI die PLZ ein, wird automatisch die entsprechende Stadt (und Land) aus der Datenbank gezogen und als Vorschlag in die entsprechenden Felder eingefügt.

2.1.3 - Adressen & Zahlungsoptionen
Die Trennung von Nutzer, Adresse und Zahlungsinformationen lässt sich leicht um eine Möglichkeit erweitern, dass ein Nutzer mehrere Zahlungsinformationen hinterlegen und diesen unterschiedliche Rechnungsadressen zuordnen kann.

2.1.4 - Länder
Derzeit wird das Land einer Adresse nicht abgefragt, hier würde sich eine Tabelle mit einem Dropdown auf FrontEnd-Seite anbieten.

=======================================================================================

2.2 - Front-End

2.2.1 - Registrierung
Ich würde die Registrierung um die Angabe einer E-Mail und eines Passwortes erweietrn, sodass Nutzer später noch ihre Daten einsehen, ändern und erweitern können.

2.2.2 - Eingabe-Verifikation & Bereinigung
Ausgehend von der Aufgabenstellung habe ich die Verifikation & Bereinigung der Nutzerdaten nur rudimentär implementiert. Für eine Produktiv-Version müssten hier wesentlich strengere Maßnahmen genutzt werden. Hier könnten die Punkte 2.1.1 und 2.1.2 gut eingebracht werden.
In der derzeitigen Implementierung wird nach einmaligem Absenden das erneute Übermitteln der Daten gesperrt. Eine Änderung in den Eingabefeldern gibt zu diesem Zeitpukt nicht die Erneute Übermittlung frei, da ohne einen Login nicht klar ist ob der Nutzer seine Daten ändern will oder aber ein neuer Nutzer angelegt werden soll.

2.2.3 - Anzeige
Die Anzeige ist entsprechend der Aufgabenstellung sehr rudimentär und könnte noch einiges an Aufhübschung vertragen.

2.2.4 - Multi-Language
Da Fleetbird auch im internationalen Raum tätig ist wäre es sinnvoll, die bevorzugte Sprache des Browsers zu erkennen und die Elemente auf der Seite in der entsprechenden Sprache (Sofern vorhanden) anzuzeigen. Hierfür müsste Datenbankseitig eine Tabelle mit implementierten Sprachen hinterlegt werden und die Tabelle 2.1.4 auf die entsprechenden Sprachen gemappt werden.

=======================================================================================

3 - Implementierung
Die Implementierung erfolgte in Anlehnung an die MVC-Struktur von Yii, da ich mich in letzteres in so kurzer Zeit nicht ausreichend einarbeiten konnte. Da ich bisher noch keine nennenswerten Erfahrungen im Bereich MVC habe mögen meine Lösungen an einigen Stellen sicher nicht dem Standard entsprechen. 